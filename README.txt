Offline  - A theme for you dead site \m/

----------------------------------------
  Setup
----------------------------------------
To make it work for offline use you have to do some fiddeling in the site settings.php
add this line to the top of your sites/YOURSITE/settings.php

  $conf['maintenance_theme'] = 'offline';

and youre ready to go offline in style. 

----------------------------------------
  Test
----------------------------------------
To test it open up 2 different browsers fx opera & firefox.
In the first browser you will login as admin and in the second you will login as a normal user.

1. Admin browser
Login to your site as admin 

Clear the theme cache (/admin/settings/performance)  down in the bottom of the page "clear cached data"

Set the site to off-line in the site-maintenance (/admin/settings/site-maintenance)
DONT lock out!!!
if youre locked out .... : http://lmgtfy.com/?q=logged+out+of+drupal+in+maintenance+mode

2. user browser
Open up the other browser and go to the site you should now se the site-offline.

if you wanna se the page when its totally offline - like in a database error:
go to settings.php
find the $db_url settings its around line 96, and changethe password etc to something else to provoke the error


----------------------------------------
  contact
----------------------------------------
http://drupal.org/project/offline

mortendk:  http://drupal.org/user/65676